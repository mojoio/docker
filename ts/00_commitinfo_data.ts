/**
 * autocreated commitinfo by @push.rocks/commitinfo
 */
export const commitinfo = {
  name: '@apiclient.xyz/docker',
  version: '1.2.5',
  description: 'Provides easy communication with Docker remote API from Node.js, with TypeScript support.'
}
