import * as plugins from '../plugins.js';

import * as interfaces from './index.js';
import { DockerNetwork } from '../classes.network.js';
import { DockerSecret } from '../classes.secret.js';
import { DockerImage } from '../classes.image.js';

export interface IServiceCreationDescriptor {
  name: string;
  image: DockerImage;
  labels: interfaces.TLabels;
  networks: DockerNetwork[];
  networkAlias: string;
  secrets: DockerSecret[];
  ports: string[];
  accessHostDockerSock?: boolean;
  resources?: {
    memorySizeMB?: number;
    volumeMounts?: plugins.tsclass.container.IVolumeMount[];
  };
}
