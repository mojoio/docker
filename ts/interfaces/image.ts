export interface IImageCreationDescriptor {
  imageUrl: string;
  imageTag?: string;
}
